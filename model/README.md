## FEniCS Growth model usage

The growthmodel relies on FEniCS and mpi4py. It is recommended to run it on a high-performance compute environment in a highly
parallelized setting. Depending on the size of your region of interest, you will need a large amount of memory. For the 
example data and parameters in this repository we used 48 cores and 120Gb of memory. 

We recommend using the provided Singularity image to run the experiments.

This directory contains a number of resources:
- `fenics_growthmodel.simg` -- Singularity image with FEniCS and required python libraries
- `Singularity_file` -- Recipe for the Singularity image, in case you want to adapt it
- `modelconfig.py` -- Contains a helper class to manage the model parameters and additional configurations
- `growthmodel.py` -- Main implementation of the FEniCS initialization and time-stepping, including helper functions to read and write NIfTI images
- `runatlasmodel.py` -- Implementation of the diffusion model in a brain atlas. Modify this file to change the model assumptions.
- `config.py` -- file to store your specific configuration in terms of file paths
- `runatlas.sh` -- example script to run the model, assuming you use Singularity and the slurm scheduler. Adapt as needed (e.g. using mpirun). Refer to the *[Singularity documentation](https://docs.sylabs.io/guides/3.3/user-guide/mpi.html)* for help.
- `normalize_dti_atlas.py` -- helper function to normalize the tensor atlas

To use the IIT Human Brain Atlas, download the following files from their repository and store them in the `data` directory:
- `IIT_GM_tissue_prob.nii.gz`
- `IIT_WM_tissue_prob.nii.gz`
- `IIT_mean_FA.nii.gz`
- `IIT_mean_tensor.nii.gz`
and run `python normalize_dti_atlas.py` to obtain the normalized tensor image. You can do this in any python environment that has SimpleITK installed, or use the Singularity image.
