# Copyright 2011-2022 Department of Radiology, Erasmus MC, Rotterdam, The Netherlands

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import sys

import numpy as np
import SimpleITK as sitk
from mpi4py import MPI as MPI_PY
from dolfin import *
from modelconfig import ModelConfig


comm = MPI_PY.COMM_WORLD
rank = comm.Get_rank()

parameters["ghost_mode"] = "shared_facet"
parameters['krylov_solver']['nonzero_initial_guess'] = True
parameters['form_compiler']['cpp_optimize'] = True
parameters['form_compiler']['optimize'] = True


class GrowthModel():

    def __init__(self, config: ModelConfig):
        self.cfg = config
        return

    def save_nifti(
            self, function: Function, fill_value: float,
            out_file: str, dim=1, datatype=float, isMeshFunction=False):
        img = sitk.ReadImage(self.cfg.reference_nifti)
        imgarray = sitk.GetArrayFromImage(img)
        x = []
        y = []
        z = []
        val = []
        for cell in cells(self.mesh):
            loc_point = cell.midpoint()
            x.append(int(loc_point.x()) + self.cfg.roi[0])
            y.append(int(loc_point.y()) + self.cfg.roi[1])
            z.append(int(loc_point.z()) + self.cfg.roi[2])
            if isMeshFunction:
                val.append(function[cell])
            else:
                val.append(function(loc_point))
        val = np.array(val, dtype=datatype)
        if len(val.shape) == 1:
            val = np.expand_dims(val, axis=1)
        indices = np.ravel_multi_index([z, y, x], imgarray.shape)
        n_indices = comm.gather(len(indices), root=0)

        if rank == 0:
            recvbuf = np.empty(sum(n_indices), dtype=int)
            all_vals = np.empty((sum(n_indices), dim), dtype=datatype)
            recvbuf_vals = np.empty(sum(n_indices), dtype=datatype)
        else:
            recvbuf = None
            recvbuf_vals = None
            all_vals = None
        comm.Gatherv(sendbuf=indices, recvbuf=(recvbuf, n_indices), root=0)
        for i in range(dim):
            sendbuf = np.array(val[:, i])
            comm.Gatherv(
                sendbuf=sendbuf, recvbuf=(recvbuf_vals, n_indices), root=0)
            if rank == 0:
                all_vals[:, i] = recvbuf_vals
        if rank == 0:
            # The returned values will contain duplicate indices
            # For simplicity, we prune these
            # from the results without considering
            # possible differences between the values
            ix_all, selection = np.unique(recvbuf, return_index=True)
            vals = all_vals[selection, :]
            components = []
            for i in range(dim):
                data_array = np.zeros(imgarray.shape)
                data_array[:] = fill_value
                data_array.flat[ix_all] = vals[:, i]
                img_out = sitk.GetImageFromArray(data_array)
                img_out.CopyInformation(img)
                components.append(img_out)
            imgout = sitk.JoinSeries(components)
            sitk.WriteImage(imgout, out_file)

    def load_from_nifti(self, fname: str):
        f = Function(self.V)

        F_dof_coordinates = self.V.tabulate_dof_coordinates()

        d = self.mesh.geometry().dim()
        F_dof_coordinates.reshape((-1, d))

        F_dof_x = np.round(F_dof_coordinates[:, 0], 0)
        F_dof_y = np.round(F_dof_coordinates[:, 1], 0)
        F_dof_z = np.round(F_dof_coordinates[:, 2], 0)

        img = sitk.ReadImage(fname)
        array = sitk.GetArrayFromImage(img)
        array = array[
            self.cfg.roi[2]:self.cfg.roi[2]+self.cfg.mesh_size[2]+1,
            self.cfg.roi[1]:self.cfg.roi[1]+self.cfg.mesh_size[1]+1,
            self.cfg.roi[0]:self.cfg.roi[0]+self.cfg.mesh_size[0]+1]
        f_values = array[
            F_dof_z.astype(int),
            F_dof_y.astype(int),
            F_dof_x.astype(int)]
        f.vector().set_local(f_values)
        return f

    def init_mesh(self):
        # Create mesh and define function space
        self.mesh = BoxMesh(
            Point(0, 0, 0),
            Point(*self.cfg.mesh_size),
            *self.cfg.mesh_size)

        # V will be the space where all scalars live,
        # such as the tumor cell density
        # dg is the space for the cell density
        self.dg = FunctionSpace(self.mesh, "DG", 1)

        self.V = FunctionSpace(self.mesh, "DG", 0)

        # DTI contains the tensors used for diffusion. Keep this at degree 1!
        self.DTI = TensorFunctionSpace(self.mesh, "P", 1)

    def note_brain_boundaries(self):
        boundary_markers = MeshFunction(
            "size_t", self.mesh,
            self.mesh.topology().dim()-1)
        boundary_markers.set_all(0)
        for c in cells(self.mesh):
            if self.tissue_type[c] == 0:
                for f in c.entities(self.mesh.topology().dim()-1):
                    boundary_markers[f] = 1

        self.db = Measure('dS', domain=self.mesh,
                          subdomain_data=boundary_markers)

        f = XDMFFile(self.mesh.mpi_comm(), os.path.join(self.cfg.local_log, 'boundaries.xdmf'))
        f.write(boundary_markers)

    def load_model(self):
        d = Function(self.DTI)
        f_dti = HDF5File(self.mesh.mpi_comm(), os.path.join(
            self.cfg.local_log, 'diffusion_tensor.h5'), 'r')
        f_dti.read(d, '/function')

        self.d = d

        tissue = Function(self.V)
        f_g = HDF5File(self.mesh.mpi_comm(), os.path.join(
            self.cfg.local_log, 'tissue.h5'), 'r')
        f_g.read(G, '/function')
        self.tissue_type = tissue

    def get_equations(self, u_n: Function):
        # Make subdomain
        dtissue = Measure('dx', domain=self.mesh,
                          subdomain_data=self.tissue_type)
        self.note_brain_boundaries()
        n = FacetNormal(self.mesh)
        h = self.mesh.hmin()

        # Variational problem at each time
        u = TrialFunction(self.dg)
        v = TestFunction(self.dg)
        ## Diffusion step
        F = u*v*dtissue(1) - self.u_n*v*dtissue(1) +\
            u*v*dtissue(2) - self.u_n*v*dtissue(2) +\
            self.cfg.dt*0.25*inner(self.d*grad(u), grad(v))*dtissue(1) +\
            self.cfg.dt*0.25*inner(self.d*grad(self.u_n), grad(v))*dtissue(1) +\
            self.cfg.dt*0.25*inner(self.d*grad(u), grad(v))*dtissue(2) +\
            self.cfg.dt*0.25*inner(self.d*grad(self.u_n), grad(v))*dtissue(2) +\
            u*v*dtissue(0) - \
            - self.cfg.dt*0.25*dot(avg(self.d*grad(v)), jump(u, n))*self.db(0) \
            - self.cfg.dt*0.25*dot(jump(v, n), avg(self.d*grad(u)))*self.db(0) \
            + self.cfg.dt/avg(h)*dot(jump(v, n), jump(u, n))*self.db(0) \
            - self.cfg.dt*0.25*dot(avg(self.d*grad(v)), jump(u_n, n))*self.db(0) \
            - self.cfg.dt*0.25*dot(jump(v, n), avg(self.d*grad(u_n)))*self.db(0) \
            + self.cfg.dt/avg(h)*dot(jump(v, n), jump(u_n, n))*self.db(0) \
            + self.cfg.dt*dot(grad(u), n)*self.db(1) + self.cfg.dt*dot(grad(u), n)*ds ### No flux on boundary

        ## Proliferation step
        R = u*v*dtissue(1) - u_n*v*dtissue(1) +\
            u*v*dtissue(2) - u_n*v*dtissue(2) -\
            self.cfg.dt*self.cfg.rho*(u_n*(1-u_n) + abs(u_n*(1-u_n)))*v*dtissue(1) - \
            self.cfg.dt*self.cfg.rho*(u_n*(1-u_n) + abs(u_n*(1-u_n)))*v*dtissue(2) +\
            u*v*dtissue(0)

        return F, R

    def log_state(self, t: float):
        self.save_nifti(self.u_n, 0, os.path.join(
            self.cfg.local_log, f"result_day_{t}.nii.gz"))
        self.save_nifti(self.tti_map, self.cfg.T, os.path.join(
            self.cfg.local_log, f"tti_day_{t}.nii.gz"))

    def log_input(self):
        sys.stdout.flush()
        self.save_nifti(
            self.tissue_type, 0,
            os.path.join(self.cfg.local_log, "tissue_seg.nii.gz"),
            datatype=int, isMeshFunction=True)
        self.save_nifti(self.d, 0, os.path.join(
            self.cfg.local_log, 'diffusion_tensor.nii.gz'), dim=9)

    def step(self):
        u = Function(self.dg)
        u.assign(self.u_n)

        solve(self.a == self.L, u, solver_parameters={
            "linear_solver": "cg", "preconditioner": "petsc_amg"})
        self.u_n.assign(u)
        
        solve(self.b == self.M, u, solver_parameters={
            "linear_solver": "cg", "preconditioner": "petsc_amg"})
        self.u_n.assign(u)

        solve(self.a == self.L, u, solver_parameters={
            "linear_solver": "cg", "preconditioner": "petsc_amg"})
        self.u_n.assign(u)

    def compute_seg(self):
        self.seg = self.u_n > 0.5
        dtissue = Measure('dx', domain=self.mesh,
                          subdomain_data=self.tissue_type)
        integral = assemble(self.seg * dtissue(2) + self.seg * dtissue(1))
        return integral

    def update_tti(self, t: float):
        indices_2 = np.where((self.u_n.vector() >= 0.5) &
                             (self.tti_map.vector() > t))[0]
        self.tti_map.vector()[indices_2] = t

    def initialize_from_config(self, sigma: float = 1.):
        a = 1/(2*(sigma**2))
        # Initialize with a gaussian distribution
        expr_str = '1*exp('
        for i in [0, 1, 2]:
            expr_str = expr_str +\
                f'-a*pow({self.cfg.init[i] - self.cfg.roi[i]}-x[{i}], 2)'
        expr_str = expr_str + ')'
        u_0 = Expression(expr_str, degree=2, a=a)
        self.u_n = interpolate(u_0, self.dg)

    def initialize_from_seg(self):
        self.u_n = self.load_from_nifti(
            os.path.join(self.cfg.inputdir, 'POSTOP_TUMOR.nii.gz'))
        self.u_n = interpolate(self.u_n, self.dg)

    
    def run_full(self):
        F, R = self.get_equations(self.u_n)

        self.a, self.L = lhs(F), rhs(F)
        self.b, self.M = lhs(R), rhs(R)

        tti_map = Expression('C', degree=1, C=self.cfg.T)
        self.tti_map = interpolate(tti_map, self.dg)
        self.run_for_T(self.cfg.T)

    def run_for_T(self, T: float, t: float = 0.):
        # log frequency is defined in actual time, not in steps
        count_to_log = self.cfg.log_freq / self.cfg.dt
        count = 0 
        
        while t <= T:
            volume = self.compute_seg()
            if self.cfg.apply_resection and volume > self.cfg.resec_vol_thr:
                self.apply_resection()
                self.log_state(t)
                self.cfg.apply_resection = False
                print("Resection applied")
            self.update_tti(t)
            if count % count_to_log == 0:
                self.log_state(t)
            sys.stdout.flush()
            self.step()
            t += self.cfg.dt
            count += 1

    def write_final_result(self):
        self.save_nifti(self.u_n, 0, os.path.join(
            self.cfg.local_log, f"result_final.nii.gz"))
        self.save_nifti(self.tti_map, self.cfg.T, os.path.join(
            self.cfg.local_log, f"ranked_result.nii.gz"))

    def apply_resection(self):
        patch = self.cfg.get_resection()
        for cell in cells(self.mesh):
            loc_point = cell.midpoint()
            x = int(loc_point.x())
            y = int(loc_point.y())
            z = int(loc_point.z())
            if patch[z, y, x] > 0.5:
                self.tissue_type[cell] = 0
        self.note_brain_boundaries()

