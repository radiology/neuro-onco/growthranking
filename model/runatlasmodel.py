# Copyright 2011-2022 Department of Radiology, Erasmus MC, Rotterdam, The Netherlands

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import argparse
import os

import numpy as np
import SimpleITK as sitk
from mpi4py import MPI as MPI_PY
from dolfin import *
from modelconfig import ModelConfig
from growthmodel import GrowthModel


comm = MPI_PY.COMM_WORLD
rank = comm.Get_rank()

class AtlasDiffusion(UserExpression):

    def __init__(self, cfg: ModelConfig, tissue: MeshFunction, mesh: Mesh):
        slice_tup = (
            slice(cfg.roi[2], cfg.roi[2] + cfg.mesh_size[2]),
            slice(cfg.roi[1], cfg.roi[1] + cfg.mesh_size[1]),
            slice(cfg.roi[0], cfg.roi[0] + cfg.mesh_size[0]))

        img_tens = sitk.ReadImage('resources/normalized_tensor_im.nii.gz')
        patch_tens = sitk.GetArrayFromImage(img_tens)[slice_tup + (slice(None), ) ]

        
        img_wm = sitk.ReadImage('resources/IIT_WM_tissue_prob.nii.gz')
        patch_wm = sitk.GetArrayFromImage(img_wm)[slice_tup]
        img_gm = sitk.ReadImage('resources/IIT_GM_tissue_prob.nii.gz')
        patch_gm = sitk.GetArrayFromImage(img_gm)[slice_tup]

        self.v0 = MeshFunction("double", mesh, mesh.topology().dim())
        self.fa = MeshFunction("double", mesh, mesh.topology().dim())

        self.c00 = MeshFunction("double", mesh, mesh.topology().dim())
        self.c01 = MeshFunction("double", mesh, mesh.topology().dim())
        self.c02 = MeshFunction("double", mesh, mesh.topology().dim())
        self.c11 = MeshFunction("double", mesh, mesh.topology().dim())
        self.c12 = MeshFunction("double", mesh, mesh.topology().dim())
        self.c22 = MeshFunction("double", mesh, mesh.topology().dim())

        for cell in cells(mesh):
            loc_point = cell.midpoint()
            x = int(loc_point.x())
            y = int(loc_point.y())
            z = int(loc_point.z())
            tensor = patch_tens[z, y, x, :]*cfg.dDTI

            if tissue[cell] < 0.5:
                self.v0[cell] = 0
                self.c00[cell] = 0
                self.c01[cell] = 0
                self.c02[cell] = 0
                self.c11[cell] = 0
                self.c12[cell] = 0
                self.c22[cell] = 0
            
            else:

                self.c00[cell] = tensor[0]
                self.c01[cell] = tensor[1]
                self.c02[cell] = tensor[2]
                self.c11[cell] = tensor[3]
                self.c12[cell] = tensor[4]
                self.c22[cell] = tensor[5]

                self.v0[cell] = (1* patch_wm[z, y, x] + cfg.ddGM * patch_gm[z, y, x]) * cfg.dI

        super().__init__(degree=1)

    def eval_cell(self, value, x, cell):
        i = cell.index
        v0 = self.v0[i]
        value[0] = self.c00[i] + v0
        value[1] = self.c01[i]
        value[2] = self.c02[i]
        value[3] = self.c01[i]
        value[4] = self.c11[i] + v0
        value[5] = self.c12[i]
        value[6] = self.c02[i]
        value[7] = self.c12[i]
        value[8] = self.c22[i] + v0

    def value_shape(self):
        return(3, 3,)


class AtlasGrowthModel(GrowthModel):

    def __init__(self, config: ModelConfig):
        super().__init__(config)
        self.cfg.reference_nifti = 'resources/IITmean_FA.nii.gz'
        return

    def read_tissue_type(self):

        tissue_type = MeshFunction(
            "size_t", self.mesh, self.mesh.topology().dim())

        img_wm = sitk.ReadImage('resources/IIT_WM_tissue_prob.nii.gz')
        img_gm = sitk.ReadImage('resources/IIT_GM_tissue_prob.nii.gz')
        patch_wm = sitk.GetArrayFromImage(img_wm)[
            self.cfg.roi[2]:self.cfg.roi[2]+self.cfg.mesh_size[2],
            self.cfg.roi[1]:self.cfg.roi[1]+self.cfg.mesh_size[1],
            self.cfg.roi[0]:self.cfg.roi[0]+self.cfg.mesh_size[0]]
        patch_gm = sitk.GetArrayFromImage(img_gm)[
            self.cfg.roi[2]:self.cfg.roi[2]+self.cfg.mesh_size[2],
            self.cfg.roi[1]:self.cfg.roi[1]+self.cfg.mesh_size[1],
            self.cfg.roi[0]:self.cfg.roi[0]+self.cfg.mesh_size[0]]

        for cell in cells(self.mesh):
            loc_point = cell.midpoint()
            x = int(loc_point.x())
            y = int(loc_point.y())
            z = int(loc_point.z())
            if patch_wm[z, y, x] > 0.5:
                tissue_type[cell] = 2
            elif patch_wm[z, y, x] + patch_gm[z, y, x] > 0.8:
                tissue_type[cell] = 1
            else:
                tissue_type[cell] = 0
        return tissue_type

    def load_imaging_data(self):
        self.tissue_type = self.read_tissue_type()
        D = AtlasDiffusion(self.cfg, self.tissue_type, self.mesh)
        D = as_tensor(D)
        self.d = interpolate(D, self.DTI)
    
    def initialize_from_point(self, point: tuple, sigma: float = 1.):
        a = 1/(2*(sigma**2))
        # Initialize with a gaussian distribution
        expr_str = '1*exp('
        for i in [0, 1, 2]:
            expr_str = expr_str +\
                f'-a*pow({point[i] - self.cfg.roi[i]}-x[{i}], 2)'
        expr_str = expr_str + ')'
        u_0 = Expression(expr_str, degree=2, a=a)
        self.u_n = interpolate(u_0, self.dg)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run a tumor growth model')
    parser.add_argument(
        '--yml', help='Yaml config file', default='config.yaml')
    parser.add_argument(
        '-x', type=int, help='onset location', default=None)
    parser.add_argument(
        '-y', type=int, help='onset location', default=None)
    parser.add_argument(
        '-z', type=int, help='onset location', default=None)
    args = parser.parse_args()

    config = ModelConfig(args.yml)
    model = AtlasGrowthModel(config)

    if args.x is None:
        print('Computing center of mass')
        seg_path = os.path.join(config.inputdir, 'BASE_TUMOR_to_ATLAS_bspline.nii.gz')
        seg_img = sitk.ReadImage(seg_path)

        shapefilter = sitk.LabelShapeStatisticsImageFilter()
        shapefilter.Execute(seg_img > 0.5)
        centroid = shapefilter.GetCentroid(1)

        index = seg_img.TransformPhysicalPointToIndex(centroid)

    else:
        index = [args.x, args.y, args.z]

    zeros = [0, 0, 0]
    padding = [2, 2, 2]

    ### Get the roi containing the whole brainmask
    left = [18, 14, 0]
    right = [163, 206, 165]
    size = np.array(config.mesh_size)

    ## Propose a box with initial point in the center
    centered_box_left = index - np.floor(size/2)
    centered_box_right = index + np.ceil(size/2)

    ## Find how much the proposed box overflows the brainmask roi
    overflow_left = np.max([zeros, left - centered_box_left], axis=0)
    overflow_right = np.max([zeros, centered_box_right - right], axis=0)

    ## the start moves forward if the brainmask allows it on the left, and backward if it allows it on the right
    start = centered_box_left + overflow_left - overflow_right
    start = np.max([zeros, start], axis=0)

    config.roi = [int(start[0]), int(start[1]), int(start[2])]
    
    model.init_mesh()

    print("Initializing diffusion and growth.")
    model.load_imaging_data()

    
    print(f"ROI dimensions: {model.cfg.roi}")

    if config.onsettype == 'segmentation':
        model.initialize_from_seg()
    else:
        model.initialize_from_point(index)
    model.log_input()
    model.run_full()
    model.write_final_result()

