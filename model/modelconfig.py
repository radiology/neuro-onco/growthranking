# Copyright 2011-2022 Department of Radiology, Erasmus MC, Rotterdam, The Netherlands

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import yaml
import os
import glob

from datetime import datetime
import SimpleITK as sitk
from mpi4py import MPI as MPI_PY
import numpy as np

comm = MPI_PY.COMM_WORLD
rank = comm.Get_rank()


def read_pts_file(filename: str) -> tuple:
    with open(filename) as f:
        points = f.read()

    lines = points.splitlines()
    line_with_points = lines[2]
    coordinates = line_with_points.split(' ')
    index = [int(s) for s in coordinates]
    return tuple(index)


def read_points_file(sourcedir: str) -> tuple:
    filename = 'center_point.pts'
    return read_pts_file(os.path.join(sourcedir, filename))


def read_roi_file(sourcedir: str) -> tuple:
    filename = 'center_point_roi.pts'
    return read_pts_file(os.path.join(sourcedir, filename))


class ModelConfig():

    def __init__(self, filename: str, init=True):

        this_file_dir = os.path.dirname(os.path.realpath(__file__))
        with open(os.path.join(this_file_dir, 'config.yaml'), 'r') as fname:
            localconfig = yaml.load(fname, Loader=yaml.FullLoader)

        self.PATH_DATA = localconfig["PATH_DATA"]
        self.PATH_LOG = localconfig["PATH_LOG"]

        with open(filename, 'r') as fname:
            self.cfg = yaml.load(fname, Loader=yaml.FullLoader)

        self.read_from_yaml()
        if init:
            self.initialize()
        else:
            self.local_log = os.path.join(self.PATH_LOG, self.cfg['directories']['logdir'])

    def initialize(self):
        dttime = datetime.now()
        timestamp = dttime.strftime("%Y%m%d-%H%M")
        self.name = self.cfg['directories']['name']

        name_for_log = f'{timestamp}_{self.name}'

        self.local_log = os.path.join(self.PATH_LOG, name_for_log)
        self.cfg['directories']['logdir'] = name_for_log

        self.logConfig()

    def get_resection(self):
        fname = 'resection_mask.nii.gz'
        img = sitk.ReadImage(os.path.join(self.inputdir, fname))
        patch = sitk.GetArrayFromImage(img)[
            self.roi[2]:self.roi[2]+self.mesh_size[2],
            self.roi[1]:self.roi[1]+self.mesh_size[1],
            self.roi[0]:self.roi[0]+self.mesh_size[0]]
        return patch

    def read_from_yaml(self):
        self.inputdir = os.path.join(
            self.PATH_DATA, self.cfg['directories']['sourcedir'])
        self.reference_nifti = os.path.join(self.inputdir, 'BASE_FA.nii.gz')
        self.dDTI = self.cfg['parameters']['dDTI']
        self.dI = self.cfg['parameters']['dI']
        self.ddGM = self.cfg['parameters']['ddGM']

        if self.cfg['onset']['type'] == 'segmentation':
            self.onsettype = 'segmentation'
        #     # left/bottom/front voxel of image patch
        else:
            self.onsettype = 'point'
            if 'optimized_init' in self.cfg:
                self.init = (
                    self.cfg['optimized_init']['x'],
                    self.cfg['optimized_init']['y'],
                    self.cfg['optimized_init']['z'])

        # Time variables
        self.dt = self.cfg['parameters']['dt']
        self.T = self.cfg['parameters']['T']
        self.log_freq = self.cfg['directories']['logfreq']

        self.mesh_size = (
            self.cfg['mesh']['mesh_size_x'],
            self.cfg['mesh']['mesh_size_y'],
            self.cfg['mesh']['mesh_size_z'])

        self.rho = self.cfg['parameters']['rho']

        if 'resection' in self.cfg and self.cfg['resection']['apply']:
            self.apply_resection = True
            self.resec_vol_thr = self.GetResectionThreshold()
        else:
            self.apply_resection = False

    def GetResectionThreshold(self):
        path_seg = self.get_seg_filename()
        img = sitk.ReadImage(path_seg)
        img = img > 0
        array = sitk.GetArrayFromImage(img)
        volume = np.sum(array)
        print(volume)
        return volume

    def get_seg_filename(self):
        fname = 'BASE_TUMOR.nii.gz'
        return os.path.join(self.inputdir, fname)

    def logConfig(self):
        if rank == 0:
            if not os.path.exists(self.local_log):
                os.mkdir(self.local_log)
            with open(os.path.join(self.local_log, 'config.yaml'), 'w+')\
                    as ymlfile:
                yaml.dump(self.cfg, ymlfile)

    def writeConfig(self, path):
        with open(path, 'w+') as ymlfile:
            yaml.dump(self.cfg, ymlfile)

    def get_path_tti(self):
        candidates = glob.glob(os.path.join(self.local_log, 'tti_day_*.nii.gz'))
        numbers = [s.split('tti_day_')[1].split('.')[0] for s in candidates]
        days = [int(s) for s in numbers]
        selected = np.argmax(days)
        return candidates[selected]

    def get_parameters_dict(self):

        return {
            'rho': self.rho,
            'dI': self.dI,
            'dDTI': self.dDTI,
            'ddGM': self.ddGM,
            'patient': os.path.basename(self.inputdir)
        }