## Preprocessing for growthmodel

This directory contains code to download and process data for personalized growth models.

There are three pipelines for the processing:
1. Downloading the data (`download_files.py`)
2. Processing and registering the pre-operative images (`register_to_atlas.py`)
3. Processing and registering the post-operative images (`register_to_atlasfollowup.py`)

These steps require a combination of publicly available software, which are combined using the fastr library. 
Please consult the [fastr documentation](https://fastr.readthedocs.io/en/stable/) to adapt the processing to your needs.

The following software is required and should be runnable from the command line:
- [fsl](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki)
- [elastix](https://elastix.lumc.nl/)