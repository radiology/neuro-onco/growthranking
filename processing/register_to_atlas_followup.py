# Copyright 2011-2022 Department of Radiology, Erasmus MC, Rotterdam, The Netherlands

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import fastr
import os
import argparse

def create_and_run_network(fastr_source_data, sink):
    network = fastr.create_network(id='register_to_atlas_followup')
    limit = fastr.core.resourcelimit.ResourceLimit(memory='3G')
    limit_highm = fastr.core.resourcelimit.ResourceLimit(memory='15G')

    source_elastix_params_rigid = network.create_source(
        'ElastixParameterFile', id='parameters_rigid'
        )
    source_elastix_params_affine = network.create_source(
        'ElastixParameterFile', id='parameters_affine'
        )
    source_elastix_params_bspline = network.create_source(
        'ElastixParameterFile', id='parameters_bspline'
        )
    
    source_atlas_t1 = network.create_source(
        'NiftiImageFileCompressed', id=f'ATLAS_T1')

    # Register FOLLOWUP to atlas 
    source_followup_flair = network.create_source(
        'NiftiImageFileCompressed', id=f'FOLLOWUP_FLAIR'
        )
    source_followup_tumor = network.create_source(
        'NiftiImageFileCompressed', id=f'FOLLOWUP_FLAIR_TUMOR'
        )
    source_followup_brainmask = network.create_source(
        'NiftiImageFileCompressed', id=f'FOLLOWUP_FLAIR_BRAINMASK'
        )

    fsl_mask_flair = network.create_node(
            'fsl/FSLMaths:5.0.9', tool_version='0.2',
            id=f'apply_mask_followup_flair')
    source_followup_flair.output >> fsl_mask_flair.inputs['image1']
    ['-mas'] >> fsl_mask_flair.inputs['operator1']
    source_followup_brainmask.output >>\
        fsl_mask_flair.inputs['image2']

    # segment tissue
    fsl_fast = network.create_node(
        'fsl/Fast:5.0.9', tool_version='0.1', id=f'fast_flair',
        resources=limit_highm)
    [0.3] >> fsl_fast.inputs['mrfbeta']
    [6] >> fsl_fast.inputs['classes']
    fsl_mask_flair.outputs['output_image'] >> fsl_fast.inputs['image']

    ## Register to atlas

    elastix_flair_to_atlas_rigid = network.create_node(
        'elastix/Elastix:4.8', tool_version='0.2', id='flair_to_atlas_rigid',
        resources=limit)
    source_elastix_params_rigid.output >>\
        elastix_flair_to_atlas_rigid.inputs['parameters']
    fsl_mask_flair.outputs['output_image'] >> elastix_flair_to_atlas_rigid.inputs['moving_image']
    source_atlas_t1.output >> elastix_flair_to_atlas_rigid.inputs['fixed_image']

    elastix_flair_to_atlas_affine = network.create_node(
        'elastix/Elastix:4.8', tool_version='0.2', id='flair_to_atlas_affine',
        resources=limit)
    source_elastix_params_affine.output >>\
        elastix_flair_to_atlas_affine.inputs['parameters']
    elastix_flair_to_atlas_rigid.outputs['transform'] >>\
        elastix_flair_to_atlas_affine.inputs['initial_transform']
    fsl_mask_flair.outputs['output_image'] >> elastix_flair_to_atlas_affine.inputs['moving_image']
    source_atlas_t1.output >> elastix_flair_to_atlas_affine.inputs['fixed_image']

    transformix_flair_to_atlas_rigid = network.create_node(
            'elastix/Transformix:4.8', tool_version='0.2',
            id=f'transformix_flair_to_atlas_rigid')
    elastix_flair_to_atlas_rigid.outputs['transform'] >>\
        transformix_flair_to_atlas_rigid.inputs['transform']
    fsl_mask_flair.outputs['output_image'] >>\
        transformix_flair_to_atlas_rigid.inputs['image']

    sink_flair_to_atlas_rigid = network.create_sink(
        'NiftiImageFileCompressed', id=f'FOLLOWUP_FLAIR_to_atlas_rigid')
    transformix_flair_to_atlas_rigid.outputs['image'] >>\
        sink_flair_to_atlas_rigid.input

    transformix_flair_to_atlas_affine = network.create_node(
            'elastix/Transformix:4.8', tool_version='0.2',
            id=f'transformix_flair_to_atlas_affine')
    elastix_flair_to_atlas_affine.outputs['transform'] >>\
        transformix_flair_to_atlas_affine.inputs['transform']
    fsl_mask_flair.outputs['output_image'] >>\
        transformix_flair_to_atlas_affine.inputs['image']

    sink_flair_to_atlas_affine = network.create_sink(
        'NiftiImageFileCompressed', id=f'FOLLOWUP_FLAIR_to_atlas_affine')
    transformix_flair_to_atlas_affine.outputs['image'] >>\
        sink_flair_to_atlas_affine.input

    transformix_tumor_to_atlas_rigid = network.create_node(
            'elastix/Transformix:4.8', tool_version='0.2',
            id=f'transformix_tumor_to_atlas_rigid')
    elastix_flair_to_atlas_rigid.outputs['transform'] >>\
        transformix_tumor_to_atlas_rigid.inputs['transform']
    source_followup_tumor.output >>\
        transformix_tumor_to_atlas_rigid.inputs['image']

    sink_tumor_to_atlas_rigid = network.create_sink(
        'NiftiImageFileCompressed', id=f'FOLLOWUP_TUMOR_to_atlas_rigid')
    transformix_tumor_to_atlas_rigid.outputs['image'] >>\
        sink_tumor_to_atlas_rigid.input

    transformix_tumor_to_atlas_affine = network.create_node(
            'elastix/Transformix:4.8', tool_version='0.2',
            id=f'transformix_tumor_to_atlas_affine')
    elastix_flair_to_atlas_affine.outputs['transform'] >>\
        transformix_tumor_to_atlas_affine.inputs['transform']
    source_followup_tumor.output >>\
        transformix_tumor_to_atlas_affine.inputs['image']

    sink_tumor_to_atlas_affine = network.create_sink(
        'NiftiImageFileCompressed', id=f'FOLLOWUP_TUMOR_to_atlas_affine')
    transformix_tumor_to_atlas_affine.outputs['image'] >>\
        sink_tumor_to_atlas_affine.input

    elastix_flair_to_atlas_bspline = network.create_node(
        'elastix/Elastix:4.8', tool_version='0.2', id='flair_to_atlas_bspline',
        resources=limit)
    source_elastix_params_bspline.output >>\
        elastix_flair_to_atlas_bspline.inputs['parameters']
    elastix_flair_to_atlas_affine.outputs['transform'] >>\
        elastix_flair_to_atlas_bspline.inputs['initial_transform']
    fsl_mask_flair.outputs['output_image'] >> elastix_flair_to_atlas_bspline.inputs['moving_image']
    source_atlas_t1.output >> elastix_flair_to_atlas_bspline.inputs['fixed_image']
    
    transformix_flair_to_atlas_bspline = network.create_node(
            'elastix/Transformix:4.8', tool_version='0.2',
            id=f'transformix_flair_to_atlas_bspline')
    elastix_flair_to_atlas_bspline.outputs['transform'] >>\
        transformix_flair_to_atlas_bspline.inputs['transform']
    fsl_mask_flair.outputs['output_image'] >>\
        transformix_flair_to_atlas_bspline.inputs['image']

    sink_flair_to_atlas_bspline = network.create_sink(
        'NiftiImageFileCompressed', id=f'FOLLOWUP_FLAIR_to_atlas_bspline')

    transformix_flair_to_atlas_bspline.outputs['image'] >>\
        sink_flair_to_atlas_bspline.input

    transformix_seg_to_atlas_bspline = network.create_node(
            'elastix/Transformix:4.8', tool_version='0.2',
            id=f'transformix_tumor_to_atlas_bspline')
    elastix_flair_to_atlas_bspline.outputs['transform'] >>\
        transformix_seg_to_atlas_bspline.inputs['transform']
    source_followup_tumor.output >>\
        transformix_seg_to_atlas_bspline.inputs['image']

    sink_seg_to_atlas_bspline = network.create_sink(
        'NiftiImageFileCompressed', id=f'FOLLOWUP_TUMOR_to_atlas_bspline')
    transformix_seg_to_atlas_bspline.outputs['image'] >>\
        	sink_seg_to_atlas_bspline.input

    transformix_tissue_to_atlas_bspline = network.create_node(
            'elastix/Transformix:4.8', tool_version='0.2',
            id=f'transformix_tissue_to_atlas_bspline')
    elastix_flair_to_atlas_bspline.outputs['transform'] >>\
        transformix_tissue_to_atlas_bspline.inputs['transform']
    fsl_fast.outputs['segmentation'] >>\
        transformix_tissue_to_atlas_bspline.inputs['image']

    sink_tissue_to_atlas_bspline = network.create_sink(
        'NiftiImageFileCompressed', id=f'FOLLOWUP_TISSUE_to_atlas_bspline')
    transformix_tissue_to_atlas_bspline.outputs['image'] >>\
        	sink_tissue_to_atlas_bspline.input

    network.execute(fastr_source_data, sink)


def main(outdir: str, debug=False):
    vfs_outdir = fastr.vfs.path_to_url(outdir)
    resources_dir_vfs = fastr.vfs.path_to_url('../resources/')
    sink_data = {
            'FOLLOWUP_FLAIR_to_atlas_rigid': os.path.join(vfs_outdir, "ATLAS", "{sample_id}", "FOLLOWUP_FLAIR_to_atlas_rigid.nii.gz"),
            'FOLLOWUP_FLAIR_to_atlas_affine': os.path.join(vfs_outdir, "ATLAS", "{sample_id}", "FOLLOWUP_FLAIR_to_atlas_affine.nii.gz"),
            'FOLLOWUP_TUMOR_to_atlas_affine': os.path.join(vfs_outdir, "ATLAS", "{sample_id}", "FOLLOWUP_TUMOR_to_atlas_affine.nii.gz"),
            'FOLLOWUP_TUMOR_to_atlas_rigid': os.path.join(vfs_outdir, "ATLAS", "{sample_id}", "FOLLOWUP_TUMOR_to_atlas_rigid.nii.gz"),
            'FOLLOWUP_FLAIR_to_atlas_bspline': os.path.join(vfs_outdir, "ATLAS", "{sample_id}", "FOLLOWUP_FLAIR_to_atlas_bspline.nii.gz"),
            'FOLLOWUP_TUMOR_to_atlas_bspline': os.path.join(vfs_outdir, "ATLAS", "{sample_id}", "FOLLOWUP_TUMOR_to_atlas_bspline.nii.gz"),
            'FOLLOWUP_TISSUE_to_atlas_bspline': os.path.join(vfs_outdir, "ATLAS", "{sample_id}", "FOLLOWUP_TISSUE_to_atlas_bspline.nii.gz"),
    }

    source_data = {
        'parameters_rigid': os.path.join(resources_dir_vfs,
                                         "elastix_parameters.txt"),
        'parameters_affine': os.path.join(resources_dir_vfs,
                                         "elastix_parameters_affine.txt"),
        'parameters_bspline': os.path.join(resources_dir_vfs,
                                         "elastix_parameters_bspline_atlas.txt"),
        'ATLAS_T1': os.path.join(resources_dir_vfs,
                                         "IITmean_t1.nii.gz"),
        "FOLLOWUP_FLAIR": {},
        "FOLLOWUP_FLAIR_TUMOR": {},
        "FOLLOWUP_FLAIR_BRAINMASK": {}
    }

    for pat in os.listdir(os.path.join(outdir, 'SOURCE')):
        source_data['FOLLOWUP_FLAIR'][pat] = os.path.join(vfs_outdir, "SOURCE", pat, "FOLLOWUP_FLAIR.nii.gz")
        source_data['FOLLOWUP_FLAIR_TUMOR'][pat] = os.path.join(vfs_outdir, "SOURCE", pat, "FOLLOWUP_FLAIR_SEG.nii.gz")
        source_data['FOLLOWUP_FLAIR_BRAINMASK'][pat] = os.path.join(vfs_outdir, "SOURCE", pat, "FOLLOWUP_FLAIR_BRAINMASK.nii.gz")
        if debug:
            break

    create_and_run_network(source_data, sink_data)

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description="Preprocess for growthmodel")
    parser.add_argument('in_dir')
    parser.add_argument('--debug', action='store_true')

    args = parser.parse_args()

    main(args.in_dir, debug=args.debug)