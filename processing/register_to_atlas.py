
# Copyright 2011-2022 Department of Radiology, Erasmus MC, Rotterdam, The Netherlands

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import fastr
import os
import argparse

def create_and_run_network(fastr_source_data, sink):
    network = fastr.create_network(id='register_to_atlas')
    limit = fastr.core.resourcelimit.ResourceLimit(memory='3G')

    source_elastix_params_rigid = network.create_source(
        'ElastixParameterFile', id='parameters_rigid'
        )
    source_elastix_params_affine = network.create_source(
        'ElastixParameterFile', id='parameters_affine'
        )
    source_elastix_params_bspline = network.create_source(
        'ElastixParameterFile', id='parameters_bspline'
        )
    
    source_atlas_t1 = network.create_source(
        'NiftiImageFileCompressed', id=f'ATLAS_T1')

    # Register BASE to atlas 
    source_base_t1 = network.create_source(
        'NiftiImageFileCompressed', id=f'BASE_T1'
        )
    source_base_t1_brainmask = network.create_source(
        'NiftiImageFileCompressed', id=f'BASE_T1_BRAINMASK'
    )
    source_base_t2 = network.create_source(
        'NiftiImageFileCompressed', id=f'BASE_T2'
        )
    source_base_t2_tumor = network.create_source(
        'NiftiImageFileCompressed', id=f'BASE_T2_TUMOR'
        )

    fsl_mask_base_t1 = network.create_node(
            'fsl/FSLMaths:5.0.9', tool_version='0.2',
            id=f'apply_mask_base_T1')
    source_base_t1.output >> fsl_mask_base_t1.inputs['image1']
    ['-mas'] >> fsl_mask_base_t1.inputs['operator1']
    source_base_t1_brainmask.output >>\
        fsl_mask_base_t1.inputs['image2']

    fsl_binarize_tumor = network.create_node(
            'fsl/FSLMaths:5.0.9', tool_version='0.2',
            id=f'bin_mask_tumor')
    source_base_t2_tumor.output >> fsl_binarize_tumor.inputs['image1']
    ['-bin'] >> fsl_binarize_tumor.inputs['operator1']

    ## Register Base T1 to atlas

    elastix_base_t1_to_atlas_rigid = network.create_node(
        'elastix/Elastix:4.8', tool_version='0.2', id='base_t1_to_atlas_rigid',
        resources=limit)
    source_elastix_params_rigid.output >>\
        elastix_base_t1_to_atlas_rigid.inputs['parameters']
    fsl_mask_base_t1.outputs['output_image'] >> elastix_base_t1_to_atlas_rigid.inputs['moving_image']
    source_atlas_t1.output >> elastix_base_t1_to_atlas_rigid.inputs['fixed_image']

    transformix_base_t1_to_atlas_rigid = network.create_node(
            'elastix/Transformix:4.8', tool_version='0.2',
            id=f'transformix_base_t1_to_atlas_rigid')
    elastix_base_t1_to_atlas_rigid.outputs['transform'] >>\
        transformix_base_t1_to_atlas_rigid.inputs['transform']
    fsl_mask_base_t1.outputs['output_image'] >>\
        transformix_base_t1_to_atlas_rigid.inputs['image']

    sink_t1_to_atlas_rigid = network.create_sink(
        'NiftiImageFileCompressed', id=f'BASE_T1_to_atlas_rigid')
    transformix_base_t1_to_atlas_rigid.outputs['image'] >>\
        sink_t1_to_atlas_rigid.input

    elastix_base_t1_to_atlas_affine = network.create_node(
        'elastix/Elastix:4.8', tool_version='0.2', id='base_t1_to_atlas_affine',
        resources=limit)
    source_elastix_params_affine.output >>\
        elastix_base_t1_to_atlas_affine.inputs['parameters']
    elastix_base_t1_to_atlas_rigid.outputs['transform'] >>\
        elastix_base_t1_to_atlas_affine.inputs['initial_transform']
    fsl_mask_base_t1.outputs['output_image'] >> elastix_base_t1_to_atlas_affine.inputs['moving_image']
    source_atlas_t1.output >> elastix_base_t1_to_atlas_affine.inputs['fixed_image']

    transformix_base_t1_to_atlas_affine = network.create_node(
            'elastix/Transformix:4.8', tool_version='0.2',
            id=f'transformix_base_t1_to_atlas_affine')
    elastix_base_t1_to_atlas_affine.outputs['transform'] >>\
        transformix_base_t1_to_atlas_affine.inputs['transform']
    fsl_mask_base_t1.outputs['output_image'] >>\
        transformix_base_t1_to_atlas_affine.inputs['image']

    sink_t1_to_atlas_affine = network.create_sink(
        'NiftiImageFileCompressed', id=f'BASE_T1_to_atlas_affine')
    transformix_base_t1_to_atlas_affine.outputs['image'] >>\
        sink_t1_to_atlas_affine.input

    elastix_base_t1_to_atlas_bspline = network.create_node(
        'elastix/Elastix:4.8', tool_version='0.2', id='base_t1_to_atlas_bspline',
        resources=limit)
    source_elastix_params_bspline.output >>\
        elastix_base_t1_to_atlas_bspline.inputs['parameters']
    elastix_base_t1_to_atlas_affine.outputs['transform'] >>\
        elastix_base_t1_to_atlas_bspline.inputs['initial_transform']
    fsl_mask_base_t1.outputs['output_image'] >> elastix_base_t1_to_atlas_bspline.inputs['moving_image']
    source_atlas_t1.output >> elastix_base_t1_to_atlas_bspline.inputs['fixed_image']

    
    transformix_base_t1_to_atlas_bspline = network.create_node(
            'elastix/Transformix:4.8', tool_version='0.2',
            id=f'transformix_base_t1_to_atlas_bspline')
    elastix_base_t1_to_atlas_bspline.outputs['transform'] >>\
        transformix_base_t1_to_atlas_bspline.inputs['transform']
    fsl_mask_base_t1.outputs['output_image'] >>\
        transformix_base_t1_to_atlas_bspline.inputs['image']

    sink_t1_to_atlas_bspline = network.create_sink(
        'NiftiImageFileCompressed', id=f'BASE_T1_to_atlas_bspline')
    transformix_base_t1_to_atlas_bspline.outputs['image'] >>\
        sink_t1_to_atlas_bspline.input

    sink_transform_t1_to_atlas_bspline = network.create_sink(
        'ElastixTransformFile', id=f'BASE_T1_to_atlas_bspline_transform')
    elastix_base_t1_to_atlas_bspline.outputs['transform'] >>\
        sink_transform_t1_to_atlas_bspline.input

    sink_transform_t1_to_atlas_affine = network.create_sink(
        'ElastixTransformFile', id=f'BASE_T1_to_atlas_affine_transform')
    elastix_base_t1_to_atlas_affine.outputs['transform'] >>\
        sink_transform_t1_to_atlas_affine.input

    sink_transform_t1_to_atlas_rigid = network.create_sink(
        'ElastixTransformFile', id=f'BASE_T1_to_atlas_rigid_transform')
    elastix_base_t1_to_atlas_rigid.outputs['transform'] >>\
        sink_transform_t1_to_atlas_rigid.input

    ## Register T2 to T1 and transform to atlas

    elastix_base_t2_to_t1_rigid = network.create_node(
        'elastix/Elastix:4.8', tool_version='0.2', id='base_t2_to_t1',
        resources=limit)
    source_elastix_params_rigid.output >>\
        elastix_base_t2_to_t1_rigid.inputs['parameters']
    source_base_t2.output >> elastix_base_t2_to_t1_rigid.inputs['moving_image']
    source_base_t1.output >> elastix_base_t2_to_t1_rigid.inputs['fixed_image']

    transformix_base_t2_to_t1_rigid = network.create_node(
            'elastix/Transformix:4.8', tool_version='0.2',
            id=f'transformix_base_t2_to_t1_rigid')
    elastix_base_t2_to_t1_rigid.outputs['transform'] >>\
        transformix_base_t2_to_t1_rigid.inputs['transform']
    source_base_t2.output >>\
        transformix_base_t2_to_t1_rigid.inputs['image']

    fsl_mask_base_t2 = network.create_node(
            'fsl/FSLMaths:5.0.9', tool_version='0.2',
            id=f'apply_mask_base_T2')
    transformix_base_t2_to_t1_rigid.outputs['image'] >> fsl_mask_base_t2.inputs['image1']
    ['-mas'] >> fsl_mask_base_t2.inputs['operator1']
    source_base_t1_brainmask.output >>\
        fsl_mask_base_t2.inputs['image2']

    transformix_base_seg_to_t1_rigid = network.create_node(
            'elastix/Transformix:4.8', tool_version='0.2',
            id=f'transformix_base_tumor_to_t1_rigid')
    elastix_base_t2_to_t1_rigid.outputs['transform'] >>\
        transformix_base_seg_to_t1_rigid.inputs['transform']
    fsl_binarize_tumor.outputs['output_image'] >>\
        transformix_base_seg_to_t1_rigid.inputs['image']

    ## Transform to atlas bspline
    transformix_base_t2_to_atlas_bspline = network.create_node(
            'elastix/Transformix:4.8', tool_version='0.2',
            id=f'transformix_base_t2_to_atlas_bspline')
    elastix_base_t1_to_atlas_bspline.outputs['transform'] >>\
        transformix_base_t2_to_atlas_bspline.inputs['transform']
    fsl_mask_base_t2.outputs['output_image'] >>\
        transformix_base_t2_to_atlas_bspline.inputs['image']

    sink_t2_to_atlas_bspline = network.create_sink(
        'NiftiImageFileCompressed', id=f'BASE_T2_to_atlas_bspline')
    transformix_base_t2_to_atlas_bspline.outputs['image'] >>\
        	sink_t2_to_atlas_bspline.input

    transformix_base_seg_to_atlas_bspline = network.create_node(
            'elastix/Transformix:4.8', tool_version='0.2',
            id=f'transformix_base_tumor_to_atlas_bspline')
    elastix_base_t1_to_atlas_bspline.outputs['transform'] >>\
        transformix_base_seg_to_atlas_bspline.inputs['transform']
    transformix_base_seg_to_t1_rigid.outputs['image'] >>\
        transformix_base_seg_to_atlas_bspline.inputs['image']

    sink_seg_to_atlas_bspline = network.create_sink(
        'NiftiImageFileCompressed', id=f'BASE_TUMOR_to_atlas_bspline')
    transformix_base_seg_to_atlas_bspline.outputs['image'] >>\
        	sink_seg_to_atlas_bspline.input

    transformix_base_seg_to_atlas_rigid = network.create_node(
            'elastix/Transformix:4.8', tool_version='0.2',
            id=f'transformix_base_tumor_to_atlas_rigid')
    elastix_base_t1_to_atlas_rigid.outputs['transform'] >>\
        transformix_base_seg_to_atlas_rigid.inputs['transform']
    transformix_base_seg_to_t1_rigid.outputs['image'] >>\
        transformix_base_seg_to_atlas_rigid.inputs['image']

    sink_seg_to_atlas_rigid = network.create_sink(
        'NiftiImageFileCompressed', id=f'BASE_TUMOR_to_atlas_rigid')
    transformix_base_seg_to_atlas_rigid.outputs['image'] >>\
        	sink_seg_to_atlas_rigid.input

    transformix_base_seg_to_atlas_affine = network.create_node(
            'elastix/Transformix:4.8', tool_version='0.2',
            id=f'transformix_base_tumor_to_atlas_affine')
    elastix_base_t1_to_atlas_affine.outputs['transform'] >>\
        transformix_base_seg_to_atlas_affine.inputs['transform']
    transformix_base_seg_to_t1_rigid.outputs['image'] >>\
        transformix_base_seg_to_atlas_affine.inputs['image']

    sink_seg_to_atlas_affine = network.create_sink(
        'NiftiImageFileCompressed', id=f'BASE_TUMOR_to_atlas_affine')
    transformix_base_seg_to_atlas_affine.outputs['image'] >>\
        	sink_seg_to_atlas_affine.input

    network.execute(fastr_source_data, sink)


def main(outdir: str, debug=False):
    vfs_outdir = fastr.vfs.path_to_url(outdir)
    resources_dir_vfs = fastr.vfs.path_to_url('../resources/')
    sink_data = {
            'BASE_T1_to_atlas_affine': os.path.join(vfs_outdir, "ATLAS", "{sample_id}", "BASE_T1_to_ATLAS_affine.nii.gz"),
            'BASE_T1_to_atlas_bspline': os.path.join(vfs_outdir, "ATLAS", "{sample_id}", "BASE_T1_to_ATLAS_bspline.nii.gz"),
            'BASE_T2_to_atlas_bspline': os.path.join(vfs_outdir, "ATLAS", "{sample_id}", "BASE_T2_to_ATLAS_bspline.nii.gz"),
            'BASE_TUMOR_to_atlas_bspline': os.path.join(vfs_outdir, "ATLAS", "{sample_id}", "BASE_TUMOR_to_ATLAS_bspline.nii.gz"),
            'BASE_T1_to_atlas_bspline_transform': os.path.join(vfs_outdir, "ATLAS", "{sample_id}", "BASE_T1_to_ATLAS_bspline_transform.txt"),
            'BASE_T1_to_atlas_affine_transform': os.path.join(vfs_outdir, "ATLAS", "{sample_id}", "BASE_T1_to_ATLAS_affine_transform.txt"),
            'BASE_T1_to_atlas_rigid_transform': os.path.join(vfs_outdir, "ATLAS", "{sample_id}", "BASE_T1_to_ATLAS_rigid_transform.txt"),
            'BASE_T1_to_atlas_rigid': os.path.join(vfs_outdir, "ATLAS", "{sample_id}", "BASE_T1_to_ATLAS_rigid.nii.gz"),
            'BASE_TUMOR_to_atlas_rigid': os.path.join(vfs_outdir, "ATLAS", "{sample_id}", "BASE_TUMOR_to_ATLAS_rigid.nii.gz"),
            'BASE_TUMOR_to_atlas_affine': os.path.join(vfs_outdir, "ATLAS", "{sample_id}", "BASE_TUMOR_to_ATLAS_affine.nii.gz"),
    }

    source_data = {
        'parameters_rigid': os.path.join(resources_dir_vfs,
                                         "elastix_parameters.txt"),
        'parameters_affine': os.path.join(resources_dir_vfs,
                                         "elastix_parameters_affine.txt"),
        'parameters_bspline': os.path.join(resources_dir_vfs,
                                         "elastix_parameters_bspline_atlas.txt"),
        'ATLAS_T1': os.path.join(resources_dir_vfs,
                                         "IITmean_t1.nii.gz"),
        "BASE_T1": {},
        "BASE_T1_BRAINMASK": {},
        "BASE_T2": {},
        "BASE_T2_TUMOR": {},
        }

    for pat in os.listdir(os.path.join(outdir, 'SOURCE')):
        source_data['BASE_T1'][pat] = os.path.join(vfs_outdir, "SOURCE", pat, "BASE_T1.nii.gz")
        source_data['BASE_T1_BRAINMASK'][pat] = os.path.join(vfs_outdir, "SOURCE", pat, "BASE_T1_BRAINMASK.nii.gz")
        source_data['BASE_T2'][pat] = os.path.join(vfs_outdir, "SOURCE", pat, "BASE_T2.nii.gz")
        source_data['BASE_T2_TUMOR'][pat] = os.path.join(vfs_outdir, "SOURCE", pat, "BASE_SEG.nii.gz")
        if debug:
            break

    create_and_run_network(source_data, sink_data)

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description="Preprocess for growthmodel")
    parser.add_argument('in_dir')
    parser.add_argument('--debug', action='store_true')

    args = parser.parse_args()

    main(args.in_dir, debug=args.debug)