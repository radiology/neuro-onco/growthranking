# Copyright 2011-2022 Department of Radiology, Erasmus MC, Rotterdam, The Netherlands

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

## Download files from XNAT using fastr source file
## And create a new fastr source file

import argparse
import fastr
import os
import json

def main(src_file_dir, tmp_dir):

    new_fastr_source = {'brainmask': {}}
    

    for filename in os.listdir(src_file_dir):
        print(filename)
        if not '.json' in filename:
            continue
        src_file_path = os.path.join(src_file_dir, filename)

        patientid = filename[:-5]
        pat_tmp_dir = os.path.join(tmp_dir, 'SOURCE', patientid)

        with open(src_file_path) as f:
            fastr_source = json.load(f)
        sinks = {}
        followup_t1_scan = fastr_source['FOLLOWUP_T1'].split('/resources/')[0]
        followup_flair_scan = fastr_source['FOLLOWUP_FLAIR'].split('/resources/')[0]
        postop_t1_scan = fastr_source['POSTOP_T1'].split('/resources/')[0]
        base_t1_scan = fastr_source['BASE_T1'].split('/resources/')[0]
        fastr_source['FOLLOWUP_T1_SEG'] = followup_t1_scan + '/resources/MASKS/files/TUMOR_KG_GMproject.nii.gz'
        fastr_source['FOLLOWUP_T1_BRAINMASK'] = followup_t1_scan + '/resources/MASKS/files/BRAIN_KG_GMproject.nii.gz'
        fastr_source['FOLLOWUP_FLAIR_SEG'] = followup_flair_scan + '/resources/MASKS/files/TUMOR_KG_GMproject.nii.gz'
        fastr_source['FOLLOWUP_FLAIR_BRAINMASK'] = followup_flair_scan + '/resources/MASKS/files/BRAIN_KG_GMproject.nii.gz'
        fastr_source['POSTOP_T1_SEG'] = postop_t1_scan + '/resources/MASKS/files/TUMOR_KG_GMproject.nii.gz'
        fastr_source['POSTOP_T1_BRAINMASK'] = postop_t1_scan + '/resources/MASKS/files/BRAIN_KG_GMproject.nii.gz'
        fastr_source['BASE_T1_BRAINMASK'] = base_t1_scan + '/resources/MASKS/files/BRAIN_KG_GMproject.nii.gz'

        network = fastr.create_network(id='Download_files')
        for key, value in fastr_source.items():
            
            sink_key = key + '_sink'

            if not key in new_fastr_source:
                new_fastr_source[key] = {}

            if key == 'BASE_DTI':
                source_node = network.create_source('Directory', id=key)
                sinks[sink_key] = os.path.join(fastr.vfs.path_to_url(pat_tmp_dir), key)
                sink_node = network.create_sink('Directory', id=sink_key)
                new_fastr_source[key][patientid] = fastr.vfs.path_to_url(os.path.join(pat_tmp_dir, f'{key}'))
            else:
                source_node = network.create_source('NiftiImageFileCompressed', id=key)
                sinks[sink_key] = os.path.join(fastr.vfs.path_to_url(pat_tmp_dir), f'{key}.nii.gz')
                sink_node = network.create_sink('NiftiImageFileCompressed', id=sink_key)
                new_fastr_source[key][patientid] = fastr.vfs.path_to_url(os.path.join(pat_tmp_dir, f'{key}.nii.gz'))

            source_node.output >> sink_node.input
        
            
            

        network.execute(fastr_source, sinks)



if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description="Download files from xnat using fastr source")
    parser.add_argument('source_file_dir')
    parser.add_argument('temp_dir')

    args = parser.parse_args()


    main(args.source_file_dir, args.temp_dir)