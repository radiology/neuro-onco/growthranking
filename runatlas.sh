#!/bin/sh
#SBATCH -p thin #Select partition
#SBATCH -t 5:00:00
#SBATCH -o log/%j_out.log  # send stdout to outfile
#SBATCH -e log/%j_err.log   # send stderr to errfile
#SBATCH --tasks-per-node 48
#SBATCH -N 1
#SBATCH --mem=120G

module load 2021
module load MPICH/3.4.2-GCC-10.3.0

cd growthranking/
srun -n 48 singularity exec model/fenics_growthmodel.simg python3 model/runatlasmodel.py --yml ${1}
