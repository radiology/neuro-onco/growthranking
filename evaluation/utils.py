# Copyright 2011-2022 Department of Radiology, Erasmus MC, Rotterdam, The Netherlands

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
import SimpleITK as sitk
import os
from sklearn.metrics import average_precision_score, precision_recall_curve, precision_score


def get_local_precision_recall(rankarray: np.array, seg_array: np.array, inclusion_mask: np.array):
    
    (precision, recall, thresholds) = get_precision_recall_curve(
        rankarray, seg_array, inclusion_mask)

    ranks_reshaped = rankarray.reshape(rankarray.shape[0]*rankarray.shape[1]*rankarray.shape[2])
    indices = np.searchsorted(thresholds, 0 - ranks_reshaped, side='right')
    prec_values = precision[indices]
    prec_values = prec_values.reshape(rankarray.shape[0], rankarray.shape[1], rankarray.shape[2])
    recall_values = recall[indices]
    recall_values = recall_values.reshape(rankarray.shape[0], rankarray.shape[1], rankarray.shape[2])
    
    return prec_values, recall_values

def get_average_precision(rankarray: np.array, seg_array: np.array, inclusion_mask: np.array):
    values, reference_values = get_masked_scores(rankarray, seg_array, inclusion_mask)
    ap = average_precision_score(reference_values, values)
    return ap

def get_precision_recall_curve(rankarray: np.array, seg_array: np.array, inclusion_mask: np.array):
    values, reference_values = get_masked_scores(rankarray, seg_array, inclusion_mask)
    (precision, recall, thresholds) = precision_recall_curve(reference_values, values)
    return precision, recall, thresholds

def get_masked_scores(rankarray: np.array, seg_array: np.array, inclusion_mask: np.array):
    values = 0 - rankarray[inclusion_mask]
    reference_values = seg_array[inclusion_mask]
    return values, reference_values

def get_exclusion_mask(seg_img, atlas_tissue_img, tissue_img=None):

    exclusion_img = atlas_tissue_img < 0.5

    if tissue_img is not None:
        exclusion_img_tissue = (tissue_img + sitk.Cast(seg_img, tissue_img.GetPixelIDValue())) < 2
        exclusion_img = exclusion_img + exclusion_img_tissue > 0.5
    return exclusion_img

def get_dscv(rankarray: np.array, seg_array: np.array, inclusion_mask: np.array):
    values, reference_values = get_masked_scores(rankarray, seg_array, inclusion_mask)
    sorted_indices = np.argsort(values, axis=None)
    reference_volume = np.sum(reference_values)
    bin_values = np.zeros(values.shape)
    bin_values[sorted_indices[-int(reference_volume):]] = 1
    return precision_score(reference_values, bin_values)

def get_seg_at_volume(ranked: np.array, vol: float):
    sorted_indices = np.argsort(ranked, axis=None)
    selected_indices = sorted_indices[0:int(vol)]
    selected_indices = np.unravel_index(selected_indices, ranked.shape)
    result = np.zeros(ranked.shape)
    result[selected_indices] = 1
    return result

def get_seg_image_at_volume(ranked: sitk.Image, vol: float):
    ranked_array = sitk.GetArrayFromImage(ranked)
    result_array = get_seg_at_volume(ranked_array, vol)
    seg_img = sitk.GetImageFromArray(result_array)
    seg_img.CopyInformation(ranked)
    seg_img = sitk.Cast(seg_img, sitk.sitkUInt8)
    return seg_img


def get_evaluation_data_from_paths(logdir, seg_path, tissue_path=None):
    tissue_img_atlas_path = os.path.join(logdir, 'tissue_seg.nii.gz')

    tissue_img_atlas = sitk.ReadImage(tissue_img_atlas_path)
    seg_img = sitk.ReadImage(seg_path) > 0.5

    if tissue_path is not None:
        tissue_img = sitk.ReadImage(tissue_path)
        exclusion_img = get_exclusion_mask(seg_img, tissue_img_atlas, tissue_img)
    else:
        exclusion_img = get_exclusion_mask(seg_img, tissue_img_atlas)
    
    tti_path = os.path.join(logdir, 'ranked_result.nii.gz')
    if not os.path.exists(tti_path):
        print('No final ranking found.')
        return 'nan', 'nan', 'nan'
    
    tti_img = sitk.ReadImage(tti_path)
    tti_arr = sitk.GetArrayFromImage(tti_img)
    
    and_filter = sitk.AndImageFilter()
    inclusion_img = 1 - exclusion_img
    inclusion_array = sitk.GetArrayFromImage(inclusion_img) > 0.5
    seg_img = and_filter.Execute(seg_img, inclusion_img > 0.5)
    seg_array = sitk.GetArrayFromImage(seg_img)

    tti_arr = sitk.GetArrayFromImage(tti_img)
    return tti_arr, seg_array, inclusion_array

def evaluate_with_seg(logdir, seg_path, tissue_path=None):
    tti_arr, seg_array, inclusion_array = get_evaluation_data_from_paths(logdir, seg_path, tissue_path)
    
    ap = get_average_precision(tti_arr, seg_array, inclusion_array)
    dsc_v = get_dscv(tti_arr, seg_array, inclusion_array)

    return dsc_v, ap
