## Evaluation and visualization for growthmodels using the average precision

This directory contains code and jupyter notebooks to generate the results

Each notebook can be run individually, 
provided you have downloaded the dataset (make sure to change the paths in the file) and the atlas images to the resources folder.

