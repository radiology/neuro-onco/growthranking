# Copyright 2011-2022 Department of Radiology, Erasmus MC, Rotterdam, The Netherlands

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
import matplotlib.pyplot as plt


def plot_seg(background: np.array, seg: np.array, result: np.array, cut_coords: tuple):
    fig, axs = plt.subplots(1,2, figsize=(20,20))
    fig.patch.set_facecolor('black')
    axs[0].imshow(background[cut_coords[0], :, :], cmap='gray')
    axs[0].contour(seg[cut_coords[0], :, :], levels=[0.5], colors='g', linewidths=3)
    axs[0].contour(result[cut_coords[0], :, :], levels=[0.5], colors='r', linewidths=3)
    axs[0].axvline(x=cut_coords[2],color='white', linestyle=':')
    axs[0].axhline(y=cut_coords[1],color='white', linestyle=':')
    axs[0].invert_yaxis()
    
    axs[1].imshow(background[:, cut_coords[1], :], cmap='gray')
    axs[1].contour(seg[:, cut_coords[1], :], levels=[0.5], colors='g', linewidths=3)
    axs[1].contour(result[:, cut_coords[1], :], levels=[0.5], colors='r', linewidths=3)
    axs[1].axvline(x=cut_coords[2],color='white', linestyle=':')
    axs[1].axhline(y=cut_coords[0],color='white', linestyle=':')
    axs[1].invert_yaxis()