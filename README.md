# Introduction

This repository contains all code to reproduce the results for our manuscript:
K. A. van Garderen et al., "Evaluating the Predictive Value of Glioma Growth Models for Low-Grade Glioma After Tumor Resection," in IEEE Transactions on Medical Imaging, vol. 43, no. 1, pp. 253-263, Jan. 2024, doi: 10.1109/TMI.2023.3298637

Feel free to use any of the code in this repository, but please cite the above publication.

It is split into three parts, which can be used independently:
1. `processing` - pipelines for the atlas registration and related image processing
2. `model` - implementation of a glioma growth model in FEniCS
3. `evaluation` - jupyter notebooks for the evaluation and visualization of results

The software aligns with the publicy available data, which also contains all intermediate results. Therefore, you can apply each of the parts separately. 

NB: Due to potential changes made during peer review, and the limitations caused by anonimization, the source data is currently only available to reviewers. It will be published with the final article.

Since we are not allowed to redistribute the IIT human brain atlas, you need to download it from their repository (https://www.nitrc.org/projects/iit/). 
Much of the code in this repository assumes you have the following images stored in the /resources/ directory:
- `IIT_GM_tissue_prob.nii.gz`
- `IIT_WM_tissue_prob.nii.gz`
- `IITmean_t1.nii.gz`
- `IIT_mean_FA.nii.gz`
- `IIT_mean_tensor.nii.gz`


# Growth model
The growth model is a diffusion-proliferation model that uses local tissue properties of gray and white matter tissue densities and/or the anisotropic DTI-informed diffusion tensor. See [model/README.md](model/README.md) for more specific instructions to use the implementation.

We defined three different models, a baseline of homogeneous isotropic diffusion (`BASE`), a tissue-informed isotropic diffusion (`TISSUE`) and an anisotropic diffusion (`DTI`). See the figures below to see their differences in resulting shape and cell densities. These figures can be recreated using [evaluation/compare_simulations.ipynb](evaluation/compare_simulations.ipynb).


<img src="evaluation/figures/growth_pattern_0.jpg" width="400"/>

![Growth model](evaluation/figures/cell_pattern.jpg)

# Image processing
To prepare clinical patient data for use with this model, we developed two separate pipelines for baseline and follow-up data. See [processing/README.md](processing/README.md) for more details and how to use the pipelines. We find that a non-rigid registration is often needed to achieve an acceptable registration to an atlas. Parameter files for Elastix are included to reproduce these registrations. Below are examples of the registrations with both affine (green) and non-rigid deformations. Such a visualization is available for all patients in the study under [evaluation/figures/registrations/](evaluation/figures/registrations/)

<img src="evaluation/figures/registrations/reg_base_02.jpg" width="400"/>
<img src="evaluation/figures/registrations/reg_fu_02.jpg" width="400"/>


# Evaluation
For the evaluation, we propose to use to rank the voxels in order of tumor infiltration rather than set a specific cut-off point in time. An appropriate evaluation metric is then found in the average precision (AP), which is the area under the precision-recall curve. All the code used to evaluate the model results can be found under [evaluation/](evaluation/), and the visualized comparisons between models for each individual patient can be found under [figures/model_results](evaluation/evaluation/figures/model_results) and [figures/model_results](evaluation/evaluation/figures/apcurves). Below is an example of the visualized results for one patient.

![Example result visualization](evaluation/figures/precision_recall_curve.jpg)

Overall, we find that the DTI-informed model achieves a significantly higher performance than the baseline and tissue-informed model. 

<img src="evaluation/figures/patientdata_ap_boxplot.jpg" width="300"/>


